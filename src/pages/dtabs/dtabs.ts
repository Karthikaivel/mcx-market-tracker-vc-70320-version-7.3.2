import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { ComProvider } from '../../providers/com/com';

interface deviceInterface {
  id?: string
};

@IonicPage()
@Component({
  selector: 'page-dtabs',
  templateUrl: 'dtabs.html'
})
export class DtabsPage {

  public items:Array<any>=[];
  public data:any = {};
  public deviceInfo: deviceInterface = {};
  public myDate: any = new Date().toISOString();

  reportRoot = 'ReportPage';
  callsRoot = 'CallsPage';
  levelsRoot = 'LevelsPage';
  newsRoot = 'NewsPage';
  chartsRoot = 'ChartsPage';
  calendarRoot = 'CalendarPage';

  constructor(public navCtrl: NavController, private device: Device, public http: Http, private com: ComProvider, private push: Push) {
    this.data.id = this.device.uuid;
    this.http = http;
    this.expiredpage();
    this.pushSetup();
  }
  expiredpage() {
    var link = 'http://mcx.fastura.net/mcx/Subscribe-data.php';
    var myData = JSON.stringify({id: this.data.id});
    this.http.post(link, myData).map(res => res.json())
    .subscribe((data : any) =>
    {
       console.dir(data);
       console.dir(this.myDate);
       this.items = data;
       if(this.items <= this.myDate){
        this.callsRoot = 'SubscribePage';
        this.levelsRoot = 'SubscribePage';
        this.reportRoot = 'ReportPage';
        this.newsRoot = 'NewsPage';
        this.chartsRoot = 'ChartsPage';
        this.calendarRoot = 'CalendarPage';
      }
      else{
        this.callsRoot = 'CallsPage';
        this.levelsRoot = 'LevelsPage';
        this.reportRoot = 'ReportPage';
        this.newsRoot = 'NewsPage';
        this.chartsRoot = 'ChartsPage';
        this.calendarRoot = 'CalendarPage';
      }
    }, error => {
    console.log("Oooops!");
    });
  }
  shareFB(){
    this.com.shareFacebook();
  }
  sharewap(){
    this.com.shareWhatsapp();
  }
  sharetwit(){
    this.com.shareTwitter();
  }
  moreshare(){
    this.com.moreShare();
  }
  push_user(i) {
    var link = 'http://mcx.fastura.net/mcx/Push-data.php';
    var myData = JSON.stringify({id: i});
    this.http.post(link, myData).map(res => res.json())
    .subscribe((data : any) =>
    {
      console.dir("Success");
      console.dir(data);
    }, error => {
     console.log("Oooops!");
    });
  }
  
  pushSetup(){
    const options: PushOptions = {
      android: {
        senderID: '811876770665',
        sound: true,
        vibrate: true,
        icon: 'icon'
      },
      ios: {
        alert: true,
        badge: true,
        sound: true
      }  
    };
     
    const pushObject: PushObject = this.push.init(options);
         
    pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
     
    pushObject.on('registration').subscribe((registration: any) => {
      console.log('Device registered', registration);
      var i = registration.registrationId;
      console.dir(i);
      this.push_user(i);
    });
     
    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }
}
